/*
 * Copyright © 2018-2020 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import Ergo 0.0
import OAth 1.0
import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2

Page {
    id: editPage

    readonly property color bgColor: "#232323"
    readonly property color fgColor: "#efefef"

    property QtObject account: null
    readonly property bool validData: {
        return (nameField.displayText.length > 0 &&
        secretField.displayText.length >= 16);
    }

    signal close()
    signal save(Account account)

    header: Rectangle {
        id: editToolbar
        width: editPage.width
        height: units.dp(56)
        color: editPage.bgColor

        AdaptiveToolbar {
            anchors.fill: parent
            height: units.dp(56)
            leadingActions: [
                Action {
                    color: editPage.fgColor
                    iconName: "back"
                    shortcut: [StandardKey.Back, StandardKey.Cancel]
                    onTriggered: {
                        editPage.close();
                    }
                }
            ]
            trailingActions: [
                Action {
                    enabled: editPage.validData
                    color: enabled ? editPage.fgColor : "#717171"
                    iconName: "tick"
                    text: i18n.tr("Save account")
                    shortcut: StandardKey.Save
                    onTriggered: {
                        var newAccount = account;
                        if (newAccount == null) {
                            newAccount = AccountModel.createAccount();
                        }

                        newAccount.name = nameField.text;
                        newAccount.type = typeSelector.currentIndex == 1 ? Account.TOTP : Account.HOTP;
                        newAccount.secret = secretField.text;
                        newAccount.counter = parseInt(counterField.text);
                        newAccount.timeStep = parseInt(timeStepField.text);
                        newAccount.pinLength = parseInt(pinLengthField.text);
                        newAccount.algorithm = algoSelector.currentIndex << 8;
                        editPage.save(newAccount);
                    }
                }
            ]
            Text {
                anchors.fill: parent
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                text: account == null ? i18n.tr("Add account") : i18n.tr("Edit account")
                color: editPage.fgColor
                font.pixelSize: units.dp(24)
            }
        }
    }

    Rectangle {
        anchors.top: parent.top
        color: "#343434"
        width: parent.width
        height: units.dp(2)
    }

    Flickable {
        id: settingsFlickable
        anchors.fill: parent
        contentHeight: settingsColumn.height + (settingsColumn.anchors.margins * 2)

        Column {
            id: settingsColumn
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                margins: units.dp(16)
            }
            spacing: units.dp(16)

            Text {
                text: i18n.tr("Name")
                color: editPage.fgColor
                font.pixelSize: units.dp(18)
            }
            TextField {
                id: nameField
                width: parent.width
                height: units.dp(36)
                font.pixelSize: units.dp(14)
                text: account ? account.name : ""
                color: editPage.fgColor
                background: Rectangle {
                    anchors.fill: parent
                    color: "transparent"
                    border.color: "#5d5d5d"
                    radius: units.dp(10)
                }
                selectByMouse: true
                placeholderText: i18n.tr("Enter the account name")
                inputMethodHints: Qt.ImhNoPredictiveText
            }

            Text {
                text: i18n.tr("Type")
                color: editPage.fgColor
                font.pixelSize: units.dp(18)
            }

            ComboBox {
                id: typeSelector
                width: parent.width
                height: units.dp(36)
                editable: false
                model: [i18n.tr("Counter based"), i18n.tr("Time based")]
                currentIndex: account && account.type === Account.HOTP ? 0 : 1

                delegate: ItemDelegate {
                    height: typeSelector.height - units.dp(8)
                    width: typeSelector.width - units.dp(12)
                    x: units.dp(4)
                    contentItem: Text {
                        anchors.fill: parent
                        leftPadding: units.dp(4)
                        color: editPage.fgColor
                        text: modelData
                        font.bold: typeSelector.highlightedIndex === index
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: units.dp(14)
                    }
                    highlighted: false
                }

                indicator: Icon {
                    id: downIndicator
                    x: typeSelector.width - width * 2
                    y: typeSelector.topPadding + (typeSelector.availableHeight - height) / 2
                    height: typeSelector.height / 2
                    width: height
                    name: "go-down"
                    color: editPage.fgColor
                }

                background: Rectangle {
                    anchors.fill: parent
                    color: "#232323"
                    border.color: "#5d5d5d"
                    border.width: units.dp(2)
                    radius: units.dp(10)
                }

                contentItem: Text {
                    anchors.fill: parent
                    leftPadding: units.dp(16)
                    padding: units.dp(4)
                    color: editPage.fgColor
                    text: typeSelector.displayText
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: units.dp(14)
                }

                popup: Popup {
                    y: typeSelector.height - 1
                    width: typeSelector.width
                    implicitHeight: contentItem.implicitHeight
                    padding: units.dp(2)

                    contentItem: ListView {
                        clip: true
                        implicitHeight: contentHeight
                        model: typeSelector.delegateModel
                        currentIndex: typeSelector.highlightedIndex
                        delegate: typeSelector.delegate
                    }

                    background: Rectangle {
                        width: typeSelector.width
                        implicitHeight: contentItem.implicitHeight
                        color: "#343434"
                        border.color: editPage.bgColor
                        border.width: units.dp(2)
                        radius: units.dp(10)
                    }
                }
            }

            ComboBox {
                id: algoSelector
                width: parent.width
                height: units.dp(36)
                editable: false
                visible: typeSelector.currentIndex == 1
                model: ["SHA-1", "SHA-256", "SHA-512"]
                currentIndex: account ? account.algorithm >> 8 : 0

                indicator: Icon {
                    x: typeSelector.width - width * 2
                    y: typeSelector.topPadding + (typeSelector.availableHeight - height) / 2
                    height: typeSelector.height / 2
                    width: height
                    name: "go-down"
                    color: editPage.fgColor
                }
            }

            Text {
                text: i18n.tr("Key")
                color: editPage.fgColor
                font.pixelSize: units.dp(18)
            }
            TextField {
                id: secretField
                width: parent.width
                height: units.dp(36)
                font.pixelSize: units.dp(14)
                text: account ? account.secret : ""
                color: editPage.fgColor
                background: Rectangle {
                    anchors.fill: parent
                    color: "transparent"
                    border.color: "#5d5d5d"
                    radius: units.dp(10)
                }
                selectByMouse: true
                wrapMode: Text.WrapAnywhere
                // TRANSLATORS: placeholder text in key textfield
                placeholderText: i18n.tr("Enter the 16 or 32 digit key")
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
            }
            Row {
                width: parent.width
                spacing: units.dp(4)
                visible: typeSelector.currentIndex == 0

                Text {
                    text: i18n.tr("Counter")
                    anchors.verticalCenter: parent.verticalCenter
                    color: editPage.fgColor
                    font.pixelSize: units.dp(18)
                }
                TextField {
                    id: counterField
                    text: account ? account.counter : 0
                    width: parent.width - x
                    height: units.dp(36)
                    font.pixelSize: units.dp(14)
                    inputMask: "0009"
                    inputMethodHints: Qt.ImhDigitsOnly
                    color: editPage.fgColor
                    background: Rectangle {
                        anchors.fill: parent
                        color: "transparent"
                        border.color: "#5d5d5d"
                        radius: units.dp(10)
                    }
                    selectByMouse: true
                }
            }
            Row {
                width: parent.width
                spacing: units.dp(4)
                visible: typeSelector.currentIndex == 1

                Text {
                    text: i18n.tr("Time step")
                    anchors.verticalCenter: parent.verticalCenter
                    color: editPage.fgColor
                    font.pixelSize: units.dp(18)
                }
                TextField {
                    id: timeStepField
                    text: account ? account.timeStep : 30
                    width: parent.width - x
                    height: units.dp(36)
                    font.pixelSize: units.dp(14)
                    inputMask: "0009"
                    inputMethodHints: Qt.ImhDigitsOnly
                    color: editPage.fgColor
                    background: Rectangle {
                        anchors.fill: parent
                        color: "transparent"
                        border.color: "#5d5d5d"
                        radius: units.dp(10)
                    }
                    selectByMouse: true
                }
            }
            Row {
                width: parent.width
                spacing: units.dp(4)

                Text {
                    text: i18n.tr("PIN length")
                    anchors.verticalCenter: parent.verticalCenter
                    color: editPage.fgColor
                    font.pixelSize: units.dp(18)
                }
                TextField {
                    id: pinLengthField
                    text: account ? account.pinLength : 6
                    width: parent.width - x
                    height: units.dp(36)
                    font.pixelSize: units.dp(14)
                    inputMask: "0D"
                    inputMethodHints: Qt.ImhDigitsOnly
                    color: editPage.fgColor
                    background: Rectangle {
                        anchors.fill: parent
                        color: "transparent"
                        border.color: "#5d5d5d"
                        radius: units.dp(10)
                    }
                    selectByMouse: true
                }
            }
            Item {
                width: parent.width
                height: Qt.inputMethod.keyboardRectangle.height
            }
        }
    }

}
